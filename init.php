<?
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", "CheckCounter");
function CheckCounter(&$arFields){
    if ($arFields["IBLOCK_ID"]==2) {
        global $APPLICATION;
        $res = CIBlockElement::GetByID($arFields["ID"]);
        if ($ar_res = $res->GetNext()) {
            if ($ar_res["SHOW_COUNTER"] > 1 && $arFields["ACTIVE"] == "N") {
                $APPLICATION->throwException("Товар невозможно деактивировать, у него ".$ar_res["SHOW_COUNTER"]." просмотров");
                return false;
            }
        }
    }
}
AddEventHandler("main", "OnBeforeUserUpdate","CheckUserGroup");
AddEventHandler("main", "OnBeforeUserAdd","CheckUserGroup");
function CheckUserGroup(&$arFields){
    $contentGroup=7;  //ID  группы контент-редакторы
    $arGroups = CUser::GetUserGroup($arFields["ID"]);  //получаем текущие группы пользователя и собираем их ID в один массив
    foreach($arFields["GROUP_ID"] as $arUserGroup){
        $arUserGroups[]=$arUserGroup["GROUP_ID"];
    }
    $arUsers = CGroup::GetGroupUser($contentGroup);  //Получаем всех пользователей группы и добавляем в 1 массив их email
    foreach ($arUsers as $arUserID){
        $rsUser = CUser::GetByID($arUserID);
        $arUser = $rsUser->Fetch();
        $arRecievers[] = $arUser["EMAIL"];
    }
    if(in_array($contentGroup,$arUserGroups)&&!in_array($contentGroup,$arGroups)&&count($arRecievers) > 0){
        $arEventFields = array(
            "USER_LOGIN" => $arFields["LOGIN"],
            "EMAIL" => implode(", ", $arRecievers),
        );
        CEvent::Send("CONTENT_GROUP_USER_ADD", "s1", $arEventFields);
    }
}
?>