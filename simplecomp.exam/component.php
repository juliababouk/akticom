<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("iblock");
$news_res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" =>$arParams["NEWS_IBLOCK_ID"]), false, false, Array("ID","IBLOCK_ID","NAME","DATE_ACTIVE_FROM","DATE_CREATE"));
while ($ob = $news_res->GetNextElement()) {
    $arNewsItems = $ob->GetFields();
    //echo "<pre>";print_r($arNewsItems);echo "</pre>";
    $arResult[$arNewsItems["ID"]]=$arNewsItems;
    $section_list = CIBlockSection::GetList(Array(), Array('IBLOCK_ID'=>$arParams["CATALOG_IBLOCK_ID"],$arParams["CATALOG_UF_PROPERTY"]=>$arNewsItems["ID"]), false,Array ("ID","NAME"));
    while($arSections = $section_list->GetNext()){
        $arResult[$arNewsItems["ID"]]["SECTIONS"][$arSections["ID"]]=$arSections["NAME"];
        //echo "<pre>";print_r($arSections);echo "</pre>";
        $arSelect = Array();//IBLOCK_ID и ID обязательно должны быть указаны, см. описание arSelectFields выше
        $arFilter = Array("IBLOCK_ID"=>$arParams["CATALOG_IBLOCK_ID"],"SECTION_ID"=>$arSections["ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y",);
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while($ob = $res->GetNextElement()){
            $arFields = $ob->GetFields();
            $arResult[$arNewsItems["ID"]]["ITEMS"][$arFields["ID"]]=$arFields;
            $arProps = $ob->GetProperties();
            $arResult[$arNewsItems["ID"]]["ITEMS"][$arFields["ID"]]["PROPERTIES"]=$arProps;
            $ar_price = CPrice::GetBasePrice($arFields["ID"]);
            $arResult[$arNewsItems["ID"]]["ITEMS"][$arFields["ID"]]["BASE_PRICE"]=$ar_price;
        }

    }
    $ar_count +=count( $arResult[$arNewsItems["ID"]]["ITEMS"]);
    //echo count( $arResult[$arNewsItems["ID"]]["ITEMS"]);
}
echo $arResult["COUNT_ITEMS"];
$APPLICATION->SetTitle(GetMessage("TITLE_TEXT"). $ar_count);
$this->IncludeComponentTemplate();
?>