<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();?>
    <h1><?$APPLICATION->ShowTitle()?></h1>
    <div class="item-list">
        <?foreach ($arResult as $arNews){?>
            <div class="item-section">
                <p><b><?=$arNews["NAME"]?></b> -
                    <?if (strlen($arNews["DATE_ACTIVE_FROM"])>0){
                        echo ConvertDateTime($arNews["DATE_ACTIVE_FROM"], "DD.MM.YYYY","ru");
                    }else{
                        echo ConvertDateTime($arNews["DATE_CREATE"], "DD.MM.YYYY","ru");
                    }?>
                 (<?=implode(",", $arNews["SECTIONS"]);?>)
                </p>
                <div class="item">
                    <?foreach ($arNews["ITEMS"] as $arItem){?>
                        <p><?echo $arItem["NAME"];
                            if (trim($arItem["BASE_PRICE"]["PRICE"])!="") {
                                echo " - " . $arItem["BASE_PRICE"]["PRICE"];
                            }
                        if (trim($arItem["PROPERTIES"]["MATERIAL"]["VALUE"])!="") {
                                echo " - " . $arItem["PROPERTIES"]["MATERIAL"]["VALUE"];
                        }
                        if (trim($arItem["PROPERTIES"]["ARTICLE"]["VALUE"])!="") {
                            echo " - " . $arItem["PROPERTIES"]["ARTICLE"]["VALUE"];
                        }
                        ?></p>
                    <?}?>
                </div>
            </div>
         <?}?>
    </div>




<?//echo "<pre>";print_r($arResult);echo "</pre>";?>